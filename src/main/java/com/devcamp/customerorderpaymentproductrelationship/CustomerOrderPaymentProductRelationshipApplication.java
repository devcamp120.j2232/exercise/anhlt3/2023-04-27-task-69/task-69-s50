package com.devcamp.customerorderpaymentproductrelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerOrderPaymentProductRelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerOrderPaymentProductRelationshipApplication.class, args);
	}

}
